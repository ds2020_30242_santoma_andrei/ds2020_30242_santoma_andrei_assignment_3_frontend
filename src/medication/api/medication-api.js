import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    getAllMedications: '/getmedications',
    medicationTaken: '/medicationtaken',
    medicationNotTaken: '/medicationnottaken',
};


function getAllMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllMedications , {
        method: 'GET',
        headers : {
            'Accept': 'application/json',
        }
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function medicationTaken(name, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationTaken + '?name=' + name , {
        method: 'POST',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function medicationNotTaken(name, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationNotTaken + '?name=' + name , {
        method: 'POST',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getAllMedications,
    medicationTaken,
    medicationNotTaken,
}