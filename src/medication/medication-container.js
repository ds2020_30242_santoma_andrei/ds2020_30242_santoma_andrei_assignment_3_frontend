import React from "react";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_MEDICATION from "./api/medication-api";
import MedicationTable from "./components/medication-table";
import 'datejs'


class MedicationContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,

        };

        this.takeMedication = this.takeMedication.bind(this);

    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000 // every second
        );
        this.fetchMedications();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        for (let i = 0; i < this.state.tableData.length; ++i) {
            let startTime = Date.parse(this.state.tableData[i]['startTime']).getTime()
            let endTime = Date.parse(this.state.tableData[i]['endTime']).getTime()
            let currentTime = (new Date()).getTime()

            if (startTime <= currentTime && currentTime <= endTime) {
                if (this.state.tableData[i]['buttonDisabled'] === true && this.state.tableData[i]['medicationTaken'] === false){
                    let tableDataCopy = this.state.tableData
                    tableDataCopy[i]['buttonDisabled'] = false
                    this.setState({tableData: tableDataCopy})
                }
            }

            if (currentTime > endTime) {
                if (this.state.tableData[i]['buttonDisabled'] === false) {
                    let tableDataCopy = this.state.tableData
                    tableDataCopy[i]['buttonDisabled'] = true
                    if (this.state.tableData[i]['medicationTaken'] === false) {

                        API_MEDICATION.medicationNotTaken(tableDataCopy[i]['medicationName'],
                            (result, status, err) => {
                                if (result !== null && status === 200) {
                                    console.log('Sent message to backend')
                                } else {
                                    this.setState(({
                                        errorStatus: status,
                                        error: err
                                    }))
                                }

                            })

                    }
                    this.setState({tableData: tableDataCopy})
                }
            }

        }
    }

    takeMedication(index) {

        let tableDataCopy = this.state.tableData


        API_MEDICATION.medicationTaken(tableDataCopy[index]['medicationName'],
            (result, status, err) => {
                if (result !== null && status === 200) {
                    console.log('Sent message to backend')
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }))
                }

            })


        console.log('Medication ' + tableDataCopy[index]['medicationName'] + ' taken')
        tableDataCopy[index]['medicationTaken'] = true
        tableDataCopy[index]['buttonDisabled'] = true
        this.setState({tableData: tableDataCopy})

    }


    fetchMedications() {
        return API_MEDICATION.getAllMedications(
            (result, status, err) => {
                if (result !== null && status === 200) {

                    for (let i = 0; i < result.length; ++i) {
                        result[i]['index'] = i
                        result[i]['buttonDisabled'] = true
                        result[i]['medicationTaken'] = false
                        let intakeHours = result[i]['intakeInterval'].split('-')
                        result[i]['startTime'] = intakeHours[0]
                        result[i]['endTime'] = intakeHours[1]
                    }

                    this.setState({tableData: result, isLoaded: true});

                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }))
                }
            }
        )
    }


    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Pill dispenser</strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <MedicationTable tableData={this.state.tableData} takeMedication={this.takeMedication}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>
            </div>
        )
    }

}

export default MedicationContainer;