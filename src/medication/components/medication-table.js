import React from "react";
import Table from "../../commons/tables/table";
import {Button} from "reactstrap";

class MedicationTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tableData: this.props.tableData
        };
    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={[
                    {
                        Header: 'Index',
                        accessor: 'index',
                        show: false,
                    },
                    {
                        Header: 'Intake interval',
                        accessor: 'intakeInterval',
                    },
                    {
                        Header: 'Name',
                        accessor: 'medicationName',
                    },
                    {
                        Header: 'ID',
                        accessor: 'medicationId',
                        show: false,
                    },
                    {
                        Header: 'Button disabled',
                        accessor: 'buttonDisabled',
                        show: false,
                    },
                    {
                        Header: 'Start time',
                        accessor: 'startTime',
                        //show: false,
                    },
                    {
                        Header: 'End time',
                        accessor: 'endTime',
                        //show: false,
                    },
                    {
                        Header: 'Medication taken',
                        accessor: 'medicationTaken',
                        show: false,
                    },
                    {
                        Header: '',
                        Cell: cell => (<Button color='primary'
                                               onClick={() => this.props.takeMedication(cell.original.index)}
                                               disabled={cell.original.buttonDisabled}>
                            Take
                        </Button>)
                    },
                ]}
                search={[]}
                pageSize={5}
            />
        )
    }
}

export default MedicationTable